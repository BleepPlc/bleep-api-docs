# Transactional Sales Report 

List all the sales by product with pagination.

Parameters: 
* start_date date 
* end_date date 
* format [json, xml]
* store_id integer
* page integer

Request Example: 

```
var request = require("request");

var options = { method: 'POST',
  url: 'http://wbo_site.m.w-bo.com/api_v1/report/trans_sales?page=2',
  headers: 
   {
     'cache-control': 'no-cache',
     'content-type': 'multipart/form-data; boundary=----WebKitFormBoundary7MA4YWxkTrZu0gW',
     token: 'SECRECT_TOKEN' 
  },
  formData: 
   { 
    start_date: '2017-01-01',
    end_date: '2017-01-28',
    format: 'json',
    store_id: '18'
    } 
  };

request(options, function (error, response, body) {
  if (error) throw new Error(error);
})
```

Response type: collection

Example: 

```
[{
  netsales:"0",
  location:"001",
  time:"14:24:36",
  date:"2017-11-03",
  last_movement_id:188,
  grosssales:"28.75",
  revenueterminal:1,
  transtype:"20",
  mediaid:"1",
  docnumber:"10001",
  user:"1",
  guest:1,
  endtime":"14:24:51"
}]
```
