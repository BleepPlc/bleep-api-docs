# Product Sales Report 

List all the sales by product with pagination.

Parameters: 
* start_date date 
* end_date date 
* format [json, xml]
* store_id integer
* page integer

Request Example: 

```
var request = require("request");

var options = { method: 'POST',
  url: 'http://wbo_site.m.w-bo.com/api_v1/report/prod_sales?page=1',
  headers: 
   { 
     'cache-control': 'no-cache',
     token: 'SECRECT_TOKEN',
     'content-type': 'application/x-www-form-urlencoded' },
  form: 
    { 
     start_date: '2018-03-02',
     end_date: '2018-03-02',
     format: 'json',
     store_id: '26' 
    }};

request(options, function (error, response, body) {
  if (error) throw new Error(error);
});
```

Response type: collection

Example: 

```
[{
  netsales:"2.04",
  vat:"0.41",
  plu:"5120",
  saletype:1,
  categorycode:"W0000000650",
  quantity:"1",
  timesold:"17:34:00",
  barcode:" ",
  grosssales:"2.45",
  description:"Sgl Gordons Gin",
  revenueterminal:1,
  discount_value:"0",
  grosssalesprice:"2.45",
  docnumber:"10003",
  netsalesprice":"2.04
}]
```
