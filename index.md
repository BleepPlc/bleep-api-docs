# Bleep API Docs

The bleep API allows you to request and send data from the WBO system, this API works mostly with JSON data format because is one of the most popular.

## Prerequisites

```
1-	The company should have WBO version 2 active
2-	Knowledge of HTTP methods and responses
```

## Authentication

Authenticate your Web API requests by providing a bearer token, which identifies a single company.

Request the authorization to use the API from the company (WBO Account) you want to integrate with. This company should be upgraded to the latest version of WBO2 v.0.8 or greater and then they will have access to the API.

In every single request, you must send the `token` added in the header of the request.
