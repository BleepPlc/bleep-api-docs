# Update User

Update the values of a single user.

Parameters:

* login: user wbo login
* email: email
* passwd: password
* fo_login: new pos login


Request Example: 

```
var request = require("request");

var options = { method: 'PUT',
  url: 'http://wbo_site.m.w-bo.com/api_v1/user/12',
  headers: 
   { 'cache-control': 'no-cache',
     token: 'SECRET_TOKEN',
     'content-type': 'application/x-www-form-urlencoded' },
  form: 
   { login: 'John',
     email: 'jj@bleep.com',
     passwd: 'newPass',
     fo_login: 'newPosLogin',
     fo_passwd: 'newPosPass' } };

request(options, function (error, response, body) {
  if (error) throw new Error(error);
});
```

Response type: JSON

Example: 

```
User updated successfully
```
