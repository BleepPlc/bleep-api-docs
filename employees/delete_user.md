# Delete Users

Remove a single user from the system.

Parameters:

* Id: user id (Integer).

Request Example: 

```
 curl -X DELETE http://wbo_site.m.w-bo.com/api_v1/user/158 -H 'token: SECRECT_TOKEN'
```

Response type: TEXT

Example: 

```
User Removed
```
