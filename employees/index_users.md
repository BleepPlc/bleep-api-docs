# List all the users

Use this route to get the list of users.

Users Fields:

* Name: employee name (varchar 60)
* Access_type: 99 means is a normal user in wbo 100 is super user (integer)
* Login: username (varchar 60) unique
* Fo_login : POS code (varchar 60)
* Passwd: wbo password (varchar 60)
* Email: employee email (varchar 50)

Request Example: 

```
curl -X GET \
  http://wbo_site.m.w-bo.com/api_v1/users \
  -H 'cache-control: no-cache' \
  -H 'content-type: application/json' \
  -H 'token: SECRECT TOKEN'
```

Response type: collection

Example: 

```
[{
  name: "Bill G",
  fo_login: "1234",
  access_type: 100,
  fo_id:  1234,
  login: billg,
  email: "bill@company_name.com",
  access_scheme: ‘Manager’,
  id: 158
}]
```
