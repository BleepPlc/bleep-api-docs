# List a single user

Use this route to get one single user.

Parameters:

* Id: user id (Integer)

Request Example: 

```
 curl -X GET http://wbo_site.m.w-bo.com/api_v1/user/158.json -H 'token: SECRECT_TOKEN'
```

Response type: JSON

Example: 

```
[{
  name: "Bill G",
  fo_login: "1234",
  access_type: 100,
  fo_id:  1234,
  login: billg,
  email: "bill@company_name.com",
  access_scheme: ‘Manager’,
  id: 158
}]
```
