# Create User

Create a new user.

Parameters:

* login: user wbo site login
* passwd: user password
* fo_login: POS login
* email: email address


Request Example: 

```
var request = require("request");

var options = { method: 'POST',
  url: 'http://wbo_site.m.w-bo.com:11015/api_v1/user',
  headers: 
   { 'cache-control': 'no-cache',
     token: 'SECRET_TOKEN',
     'content-type': 'application/json' },
  body: 
   { login: 'userLogin@123',
     passwd: 'newPassword',
     fo_login: 'newPosLogin',
     email: 'user@gmail.com' },
  json: true };

request(options, function (error, response, body) {
  if (error) throw new Error(error);
});
```

Response type: INT

When a new user is added it returns the id of the new user whit the status of 201.

Example: 

```
232
```
