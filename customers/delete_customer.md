# Delete Customer

Remove a single customer from the system.

Parameters:

* Id: customer id (Integer).

Request Example: 

```
 curl -X DELETE http://wbo_site.m.w-bo.com/api_v1/customer/672 -H 'token: SECRECT_TOKEN'
```

Response type: TEXT

Example: 

1 => success

```
1
```
