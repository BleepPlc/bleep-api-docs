# Create Customer

Create a new customer.

Parameters:

* name
* telephone 
* cellular 
* localidade 
* address 
* postal_code 
* email 
* birth_date 
* card_expiration 
* card_number 
* wm_id ('only for wm integrator')



Request Example: 

```
var request = require("request");

var options = { method: 'POST',
  url: 'http://wbo_site.m.w-bo.com/api_v1/customer',
  headers: 
   {
     'cache-control': 'no-cache',
     token: 'SECRECT_TOKEN',
     'content-type': 'application/json' 
    },
  body: 
    {
     name: 'Ray G',
     telephone: '',
     cellular: '',
     localidade: '',
     address: ',,,,',
     postal_code: '',
     email: 'ray@gmail.com',
     birth_date: '',
     card_expiration: '',
     card_number: '',
     wm_id: '231' },
     json: true 
    };

request(options, function (error, response, body) {
  if (error) throw new Error(error);

});
```

Response type: INT

When a new customer is added as result returns the new id with (201) status.

Example: 

```
232
```
