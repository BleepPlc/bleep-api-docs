# Show Customer

Use this route to display the information of a single customer.

Parameters:

* Id: customer id (Integer)

Request Example: 

```
 curl -X GET http://wbo_site.m.w-bo.com/api_v1/customer/673.json -H 'token: SECRECT_TOKEN'
```

Response type: JSON

Example: 

```
[{
  "telephone": "777 555 0005555",
  "name": "Peter",
  "birth_date": "1979-11-20",
  "fo_id": "3453",
  "email": "perter@bleep.com",
  "cellular": "32 3200 3999 888",
  "postal_code": "NW40 ASEX",
  "id": 673,
  "address": "London city ",
  "localidade": "London"
}]
```
