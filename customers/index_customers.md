# List all the customers

Show a list with all the customer.

Parameters:

* telephone integer
* name varchar(60)
* birth_date varchar(10) format "DD/MM/YYYY"
* fo_id varchar(10)
* email varchar(100)
* cellular(phone) varchar(50)
* postal_code varchar(50)
* id integer
* address varchar(60)
* localidade(city) varchar(50)


Request Example: 

```
curl -X GET http://wbo_site.m.w-bo.com/api_v1/customers -H 'token: SECRECT_TOKEN'
```

Response type: collection

Example: 

```
[{
  telephone:"",
  name:"Paola",
  birth_date:"07/02/1980",
  fo_id":"3",
  email":"",
  cellular:"",
  postal_code:"",
  id:672,
  address:"",
  localidade:""
}]
```
