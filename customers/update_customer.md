# Update Customer

Update the values of a single customer.

Parameters:

The values that you want to update from the customer.

* name
* email
* fo_id
* telephone
* cellular
* fax
* localidade
* address
* postal_code
* birth_date
* balance
* card_number
* card_expiration


Request Example: 

```
curl -X PUT \
  http://wbo_site.m.w-bo.com/api_v1/customer/702 \
  -H 'cache-control: no-cache' \
  -H 'content-type: application/json' \
  -H 'token: SECRECT_TOKEN' \
  -d '{"name":"George"}'
```

Response type: JSON

Example: 

Id of the customer updated

```
702
```
