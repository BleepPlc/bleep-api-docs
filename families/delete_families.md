#Delete Department 

To delete a department.

Parameters: 

* Id: deparment id
 
Code Example:
Please note for clarity purposes the example code had been reduced to the minimun. 

```
curl -X POST http://wbo_site.m.w-bo.com/api_v1/deparments/delete 'token: SECRECT_TOKEN'
     -F id=873
```
