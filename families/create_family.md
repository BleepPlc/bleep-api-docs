# Create Department

This creates a new department inheriting default global configuration from the group.

Parameters:

* groud_id: Id of the group where the department belongs (required)
* name: Department name (required)
* usage: this field has 3 options primary, modifier or special (required)
* fo_id: department code (Optional)
* vats: key value pair list with the store_id and vats actives note Active is (1)  (require minimum one)

Request Example: 

```
var request = require("request");

var options = { method: 'POST',
  url: 'http://wbo_site.m.w-bo.com/api_v1/departments/create',
  headers: 
   { 
     'cache-control': 'no-cache',
     token: 'SECRECT_TOKEN',
     'content-type': 'application/json' 
    },
  body: 
   { group_id: '770',
     name: 'Gabriel',
     usage: 'modifier',
     fo_id: '',
     vats: 
      [ { store_id: 18, vat1: 0, vat2: 1 },
        { store_id: 20, vat1: 1, vat2: 1 } ] },
  json: true };

request(options, function (error, response, body) {
  if (error) throw new Error(error);
});
```

Response type: JSON

Example: 

```
{
  "id":"622",
  "message":"Department added Successfully"
}
```
