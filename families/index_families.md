# List Groups and Dsepartments

List all the groups and departments.

Description:

* name: group name
* deleted: deleted ( 1 = true || 0 = false )
* id: group id
* departments: list of departments

Request Example: 

```
curl -X GET http://wbo_site.m.w-bo.com/api_v1/families-H 'token: SECRECT_TOKEN'
```

Response type: collection

Example: 

```
[{
name:"Food",
deleted:0,
id:755,
departments:[
{codes_start:"0",
 codes_end:"0",
 family_id:755,
 name:"Starters",
 deleted:0,
 id:756,
 fo_id:"W0000000630"}]
}]
```
