# Welcome Message


To know if the API is active you can go to http://wbo_site.m.w-bo.com/api_v1, this will give you the
following.

1. date    current date    
2. version API version number      
3. active  true or false, depending on the company   

example:

```
{
  date: 2017-11-29 Nov,
  version: v0.1,
  active: true,
  company: Microsoft
}
```
