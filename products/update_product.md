
# Update Products

Update the values of a single product.

Parameters:

* name: string, required: yes
* short_name: string, required: yes, max_length: 18 
* is_merc: boolean, type: numeric 
* is_sale: boolean, type: numeric 
* is_sale: number, required: yes, -> this must be the department id  
* barcode: string
* prices: list of values, price: number, required: yes, store_id: number, required: yes, vat: number,  required: yes

Request Example: 

```
var request = require("request");

var options = { method: 'PUT',
  url: 'http://wbo_site.m.w-bo.com/api_v1/product/9370',
  headers: 
   { 
     'cache-control': 'no-cache',
     token: 'SECRECT_TOKEN',
     'content-type': 'application/json' 
    },
  body: 
   { name: 'Red Bull energy 500ml',
     short_name: 'redBull sm',
     is_merc: '1',
     is_sale: '0',
     family_id: '286',
     barcode: '123213',
     prices: [ { price: 100, store_id: 13, vat: 10 } ] },
  json: true };

request(options, function (error, response, body) {
  if (error) throw new Error(error);
});
```