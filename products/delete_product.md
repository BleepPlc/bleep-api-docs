# Delete Product

Remove a single product from the system.

Parameters:

* Id: product id (Integer).

Request Example: 

```
 curl -X DELETE http://wbo_site.m.w-bo.com/api_v1/product/158 -H 'token: SECRECT_TOKEN'
```

Response type: TEXT

Example: 

1 => Success

```
1
```
