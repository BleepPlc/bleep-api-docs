# Create Product

Create a new product.

Parameters:

* name: string, required: yes
* short_name: string, required: yes, max_length: 18 
* is_merc: boolean, type: numeric 
* is_sale: boolean, type: numeric 
* is_sale: number, required: yes, -> this must be the department id  
* barcode: string
* prices: list of values, price: number, required: yes, store_id: number, required: yes, vat: number,  required: yes



Request Example: 

```
var request = require("request");

var options = { method: 'POST',
  url: 'http://wbo_site.m.w-bo.com/api_v1/product',
  headers: 
   { 
     'cache-control': 'no-cache',
     token: 'SECRECT_TOKEN',
     'content-type': 'application/json' 
    },
  body: 
   { name: 'Big coke',
     short_name: 'Coke2',
     is_merc: '1',
     is_sale: '1',
     family_id: '286',
     barcode: '234566',
     prices: [ { price: 2, store_id: 13, vat: 20 } ] },
  json: true };

request(options, function (error, response, body) {
  if (error) throw new Error(error);
});

```

Response type: INT

When a new user is added it returns the id of the new user whit the status of 201.

Example: 

```
232
```
