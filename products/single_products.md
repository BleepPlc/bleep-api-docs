# List a single products

Detailed information about the product.

Parameters:

* Id: product id (Integer)

Request Example: 

```
curl -X GET http://wbo_site.m.w-bo.com/api_v1/product/7711.json -H 'token: SECRECT_TOKEN'
```

Response type: JSON

Example: 

```
{
  "success": 1,
  "article": {
    "units_aux": null,
    "store_articles": {
        "13": {
            "negative_price": 0,
            "article_id": 5258,
            "store_id": "13",
            "price1": "0",
            "active": 1,
            "fo_id": "2003",
            "tax1": "0",
            "price0": "0",
            "price3": "0",
            "tax0": "20",
            "price2": "0",
            "store_name": "FUEL JUICE : 01 ORACLE",
            "fo_code_seq": "301126",
            "price4": "0",
            "tax2": "0"
        }
      },
      "pcm": "0",
      "photo": null,
      "promo_food_split": null,
      "is_generic": 0,
      "yield": 100,
      "tara_unidade_id": null,
      "pcu": "0",
      "aux": null,
      "round_up": "0",
      "printer_zones": [],
      "affect_stock": 1,
      "id": 5258,
      "is_sale": 1,
      "name": "Oat Bar",
      "barcode": "0",
      "stock": "0",
      "properties": {
          "addonprice": null,
          "openprice": null,
          "points": 0,
          "enableprice6": null,
          "routeprinting": null,
          "tracksystem": null,
          "activesize3": null,
          "discountlnk": null,
          "activesize7": null,
          "prefixisprice": null,
          "taretableid": null,
          "designateditem": null,
          "trackuser": null,
          "activesize1": null,
          "overrideautopricelevels": null,
          "activesize8": null,
          "enableprice8": null,
          "enableprice7": null,
          "staydownsize": null,
          "barcode": "0",
          "discountable": null,
          "securitylevel": null,
          "enablepricelevels": null,
          "prefixprice": null,
          "promopriority": null,
          "enableprice4": null,
          "activesize6": null,
          "enableprice5": null,
          "enableprice2": null,
          "activesize2": null,
          "activesize5": null,
          "article_id": 5258,
          "disabled": null,
          "mealcount": null,
          "activesize4": null,
          "promotype": null,
          "enableprice3": null,
          "preporder": 0,
          "company_id": 1,
          "enableprice1": null,
          "mixnmatch": null,
          "printofficecopy": null,
          "spellit": null,
          "kvscolor": null,
          "quantityhalo": null,
          "limitedqty": null,
          "restrictbymp": 0,
          "printhighlight": null,
          "scaleable": null,
          "rolluppricing": null
      },
      "family_id": 595,
      "is_bgeneric": 0,
      "stock_min": "0",
      "promo_drink_split": null,
      "short_name": "Oat Bar",
      "unidade_id": null,
      "dispense_factor": "0",
      "is_merc": 0,
      "brand": "",
      "tara_valor": "0",
      "size": "",
      "bfm": [
          {
              "departm_lnk": null,
              "article_id": 5258,
              "modifier_type": 0,
              "qtty": null,
              "article_lnk": null,
              "fo_id": 0
          },
          {
              "departm_lnk": null,
              "article_id": 5258,
              "modifier_type": 0,
              "qtty": null,
              "article_lnk": null,
              "fo_id": 1
          },
          {
              "departm_lnk": null,
              "article_id": 5258,
              "modifier_type": 0,
              "qtty": null,
              "article_lnk": null,
              "fo_id": 2
          },
          {
              "departm_lnk": null,
              "article_id": 5258,
              "modifier_type": 0,
              "qtty": null,
              "article_lnk": null,
              "fo_id": 3
          },
          {
              "departm_lnk": null,
              "article_id": 5258,
              "modifier_type": 0,
              "qtty": null,
              "article_lnk": null,
              "fo_id": 4
          },
          {
              "departm_lnk": null,
              "article_id": 5258,
              "modifier_type": 0,
              "qtty": null,
              "article_lnk": null,
              "fo_id": 5
          }
      ],
      "sale_unit_id": null,
      "color": "",
      "op_barcode": null,
      "last_buy": null,
      "inventory_unit_id": null,
      "markup": "0",
      "is_menu": 0,
      "gender": "",
      "company_id": 1,
      "date_updated": "2017-09-29 14:25:44.741944",
      "deleted": 0,
      "barcode2": "0",
      "fo_id": "2003",
      "gd": {
          "remote_fo_id": null,
          "company_id": 1,
          "tap_consolidation": null,
          "unidade_id": null,
          "name": "FOOD ITEMS",
          "deleted": 0,
          "fo_id": "G4",
          "family_id": null,
          "id": 593
      },
      "photo_name": null,
      "stock_max": "0",
      "expires": 0,
      "is_prod": 0
  }
}
```
