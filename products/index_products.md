# List all the products 

Request the list of product using a pagination.

Parameters:

* page number (integer)


Products Fields: 

* id serial integer
* department_name varchar
* count(total of product active) integer
* active_stores varchar
* group_name varchar
* photo base64
* photo_name varchar
* is_sale integer ( 1 = true || 0 = false)
* is_merc integer ( 1 = true || 0 = false)
* name varchar
* fo_id varchar
* family_id serial integer (department id of the product)


Request Example: 

```
var request = require("request");

var options = { method: 'GET',
  url: 'http://wbo_site.m.w-bo.com/api_v1/products?page=1',
  headers: 
   { 'cache-control': 'no-cache',
     'content-type': 'application/json',
     token: 'SECRECT_TOKEN' },
  json: true };

request(options, function (error, response, body) {
  if (error) throw new Error(error);

  console.log(body);
});

```

Response type: collection

Example: 

```
{
    "records_per_page": 30,
    "page": "1",
    "total_records": "149",
    "data": [
        {
            "department_name": "DIY Items",
            "count": "41",
            "active_stores": "15,24,25,20,21,23,19,22,27,26,29,30,32,33,35,36,37,38,13,39,40,41,42,43,44,46,47,48,49,50,51,52,53,16,34,45,14,17,18,31,28",
            "group_name": "EXTRAS",
            "photo": null,
            "is_sale": 1,
            "is_merc": 1,
            "name": "Raspberry",
            "fo_id": "1903",
            "photo_name": null,
            "family_id": 592,
            "id": 5157
        },
        ...
}
```
