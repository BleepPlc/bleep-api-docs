# List all the stores

List all the stores.

Store Fields:

* last_update date
* can_sell boolean (1 = true || 0 = false)
* name varchar
* can_buy boolean (1 = true || 0 = false)
* close_hour date
* fo_id varchar
* client_sync_now boolean (1 = true || 0 = false)
* client_last_seen date
* id serial integer
* address varchar
* fiscal_region_id serial integer
* zone_id serial integer
* fiscal_name varchar

Request Example: 

```
curl -X GET http://wbo_site.m.w-bo.com/api_v1/stores -H 'token: SECRECT_TOKEN'
```

Response type: collection

Example: 

```
[{
  last_update:null,
  can_sell:1,
  name: "Central St",
  can_buy:1,
  close_hour:"00:00:00",
  fo_id:"31",
  client_sync_now:0,
  client_last_seen:null,
  id:31,
  address: 'London',
  fiscal_region_id: 3,
  zone_id: 1,
  fiscal_name: 'central st'
}]
```
